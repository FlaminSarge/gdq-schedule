package com.shuneault.agdq2016;

import android.os.AsyncTask;
import android.util.Log;

import com.shuneault.agdq2016.objects.GameSchedule;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * Created by sebast on 01/01/16.
 */
public class WebCodeSource extends AsyncTask<Void, Integer, ArrayList<GameSchedule>> {

    public interface OnThreadProgress {
        void onProgress(Integer progress);
        void onFinish(ArrayList<GameSchedule> gameSchedule);
    }

    private static final String URL_SCHEDULE = "https://gamesdonequick.com/schedule";
    private final OnThreadProgress mListener;

    public WebCodeSource(OnThreadProgress listener) {
        mListener = listener;
    }

    @Override
    protected ArrayList<GameSchedule> doInBackground(Void... params) {
        ArrayList<GameSchedule> theSchedule = new ArrayList<>();

        try {
            Document doc = Jsoup.connect(URL_SCHEDULE).header("Cache-Control", "no-cache").get();

            Elements rows = doc.select("#runTable > tbody > tr");
            for (int i = 0; i < rows.size(); i += 2) {
                Element row1 = rows.get(i);
                Element row2 = rows.get(i + 1);
                Element elDate = row1.selectFirst(".start-time");
                String sDate = elDate == null ? "" : elDate.text();
                Element elGame = row1.selectFirst("td:nth-child(2)");
                String sGame = elGame == null ? "" : elGame.text().trim();
                boolean isCurrent = elGame != null && elGame.selectFirst("a#current") != null;
                Element elRunners = row1.selectFirst("td:nth-child(3)");
                String sRunners = elRunners == null ? "" : elRunners.text();
                Element elSetupTime = row1.selectFirst("td:nth-child(4)");
                String sSetupTime = elSetupTime == null ? "" : elSetupTime.text().trim();
                Element elRunTime = row2.selectFirst("td:nth-child(1)");
                String sRunTime = elRunTime == null ? "" : elRunTime.text().trim();
                Element elCategory = row2.selectFirst("td:nth-child(2)");
                String sCategory = elCategory == null ? "" : elCategory.text().trim();
                String sDescription = sCategory; // description matches category for now
                SimpleDateFormat sdf = new SimpleDateFormat("y-M-d'T'HH:mm:ss'Z'");
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date = sdf.parse(sDate);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                Log.i("LOG", String.format("Adding game %s at time %s -> %s", sGame, sDate, cal.getTime().toString()));
                theSchedule.add(new GameSchedule(sGame, cal, sRunners, sRunTime, sCategory, sSetupTime, sDescription, isCurrent));
            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
            Log.i("LOGCAT", "ERROR: " + e.getMessage());
        }

        Collections.sort(theSchedule, (lhs, rhs) -> lhs.getDate().compareTo(rhs.getDate()));
        return theSchedule;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        mListener.onProgress(values[0]);
    }

    @Override
    protected void onPostExecute(ArrayList<GameSchedule> gameSchedules) {
        // Only update if we have a result
        if (gameSchedules.size() > 0)
            mListener.onFinish(gameSchedules);
    }
}
