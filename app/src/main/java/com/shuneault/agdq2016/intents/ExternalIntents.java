package com.shuneault.agdq2016.intents;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import java.math.MathContext;

/**
 * Created by sebast on 06/01/16.
 */
public class ExternalIntents {

    private final Context mContext;

    private static final String PACKAGE_TWITCH = "tv.twitch.android.app";
    private static final String PACKAGE_DISCORD = "com.discord";
    private static final String URL_TWITCH_APP_STREAM = "twitch://stream/gamesdonequick";
    private static final String URL_TWITCH_WEB_STREAM = "http://www.twitch.tv/gamesdonequick";
    private static final String URL_GDQ_DONATION = "https://gamesdonequick.com/donate";
    private static final String URL_DISCORD = "https://discord.gg/gamesdonequick";

    public ExternalIntents(Context context) {
        mContext = context;
    }

    public Intent getTwitchIntent() {
        if (isPackageInstalled(PACKAGE_TWITCH, mContext)) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse(URL_TWITCH_APP_STREAM));
        } else {
            return new Intent(Intent.ACTION_VIEW, Uri.parse(URL_TWITCH_WEB_STREAM));
        }
    }

    public Intent getDonationIntent() {
        return new Intent(Intent.ACTION_VIEW, Uri.parse(URL_GDQ_DONATION));
    }

    public Intent getDiscordIntent() {
        return new Intent(Intent.ACTION_VIEW, Uri.parse(URL_DISCORD));
    }

    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
