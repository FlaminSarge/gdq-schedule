package com.shuneault.agdq2016;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shuneault.agdq2016.alarms.CurrentGameNotification;
import com.shuneault.agdq2016.alarms.ScheduledService;
import com.shuneault.agdq2016.fragments.GameScheduleFragment;
import com.shuneault.agdq2016.intents.ExternalIntents;
import com.shuneault.agdq2016.receivers.RefreshDonationBroadcastReceiver;
import com.shuneault.agdq2016.objects.GameSchedule;
import com.shuneault.agdq2016.settings.SettingsActivity;
import com.shuneault.agdq2016.settings.SettingsFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements GameScheduleFragment.OnListFragmentInteractionListener {

    private static final String SAVED_DONATION = "SAVED_DONATION";

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private TextView lblDonation;
    private LinearLayout layDonationBanner;

    private AppSingleton mySingleton;

    private String mDonations = "";

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            if (lblDonation != null) {
                refreshDonation(intent.getStringExtra(RefreshDonationBroadcastReceiver.EXTRA_DONATION_AMOUNT));
            }
        }
    };

    private void doSelectCurrentGame() {
        ArrayList<GameSchedule> games = mySingleton.getGameList();
        DateFormat df = SimpleDateFormat.getDateInstance();
        GameSchedule currentGame = null;
        for (GameSchedule game : games) {
            if (System.currentTimeMillis() > game.getDate().getTimeInMillis()) {
                currentGame = game;
            } else {
                break;
            }
        }
        for (int i = 0; i < mSectionsPagerAdapter.getTabsArray().size(); i++) {
            if (currentGame != null) {
                if (df.format(mSectionsPagerAdapter.getTabsArray().get(i).getTime()).equals(df.format(currentGame.getDate().getTime()))) {
                    //                mViewPager.setCurrentItem(i); // Workaround https://code.google.com/p/android/issues/detail?id=192834&colspec=ID%20Type%20Status%20Owner%20Summary%20Stars
                    TabLayout.Tab tabToSelect = mTabLayout.getTabAt(i);
                    if (tabToSelect != null)
                        tabToSelect.select();
                    break;
                }
            }
        }
    }

    private void refreshDonation(String donation) {
        if (!"".equals(donation)) {
            mDonations = donation;
            lblDonation.setText(String.format(getString(R.string.donation_total), donation));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Singleton
        mySingleton = AppSingleton.getInstance(this);

        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Items
        lblDonation = (TextView) findViewById(R.id.lblDonation);
        layDonationBanner = (LinearLayout) findViewById(R.id.layDonationBanner);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        // Screen orientation change
        if (savedInstanceState != null) {
            refreshDonation(savedInstanceState.getString(SAVED_DONATION));
        } else {
            WebCodeSource th = new WebCodeSource(new WebCodeSource.OnThreadProgress() {
                @Override
                public void onProgress(Integer progress) {

                }

                @Override
                public void onFinish(ArrayList<GameSchedule> gameSchedule) {
                    mySingleton.clearGames();
                    mySingleton.addAllGames(gameSchedule);
                    mySingleton.saveGameList();
                    mSectionsPagerAdapter.refreshTabs();
                    mSectionsPagerAdapter.notifyDataSetChanged();
                    if (mTabLayout.getTabCount() == 0) {
                        mTabLayout.setupWithViewPager(mViewPager);
                    }
                    doSelectCurrentGame();
                }
            });
            th.execute();

            (new AsyncTaskGetDonation(new AsyncTaskGetDonation.OnThreadProgress() {
                @Override
                public void onFinish(String donation) {
                    refreshDonation(donation);
                }
            })).execute();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(SAVED_DONATION, mDonations);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(RefreshDonationBroadcastReceiver.BROADCAST_REFRESH_DONATION);
        registerReceiver(mBroadcastReceiver, filter);

        new CurrentGameNotification().updateNotification(this, this.getIntent());

        // Show or hide the donation counter
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsFragment.PREF_DONATION_COUNTER, true)) {
            layDonationBanner.setVisibility(View.VISIBLE);
        } else {
            layDonationBanner.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
//            case R.id.mnuNow:
//                mViewPager.setCurrentItem(3, true);
//                break;
            case R.id.mnuDonate:
                startActivity(new ExternalIntents(this).getDonationIntent());
                break;
            case R.id.mnuTwitch:
                startActivity(new ExternalIntents(this).getTwitchIntent());
                break;
            case R.id.mnuSettings:
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                break;
            case R.id.mnuDiscord:
                startActivity(new ExternalIntents(this).getDiscordIntent());
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(GameSchedule item) {
        Snackbar.make(findViewById(R.id.container), item.getName() + " by " + item.getRunners(), Snackbar.LENGTH_SHORT)
                .setAction("", null).show();
    }

    @Override
    public void onListReminderCheckChanged(GameSchedule item, boolean isChecked) {
        Intent intent = new Intent(this, ScheduledService.class);
        intent.putExtra("Game", item.getName());
        intent.putExtra("GameId", item.getId());
        PendingIntent sender = PendingIntent.getService(this, item.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager mgr = (AlarmManager) getSystemService(ALARM_SERVICE);
        if (isChecked) {
            if (System.currentTimeMillis() > item.getDate().getTimeInMillis()) {
                // trigger the alarm now
                mySingleton.removeReminder(item.getName());
                mgr.set(AlarmManager.RTC_WAKEUP, item.getDate().getTimeInMillis(), sender);
                Log.i("LOGCAT", "Alarm: " + item.getDate().get(Calendar.HOUR) + ":" + item.getDate().get(Calendar.MINUTE));
            } else {
                mySingleton.addReminder(item);
            }
        } else {
            mySingleton.removeReminder(item.getName());
            mgr.cancel(sender);
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private ArrayList<Calendar> mPage = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            refreshTabs();
        }

        public void refreshTabs() {
            DateFormat df = SimpleDateFormat.getDateInstance();
            ArrayList<String> arrPages = new ArrayList<>();
            mPage.clear();
            for (GameSchedule game : mySingleton.getGameList()) {
                if (arrPages.indexOf(df.format(game.getDate().getTime())) == -1) {
                    arrPages.add(df.format(game.getDate().getTime()));
                    mPage.add(game.getDate());
                }
            }
        }

        public ArrayList<Calendar> getTabsArray() { return mPage; }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return GameScheduleFragment.newInstance(getPageTitle(position).toString());
        }

        @Override
        public int getCount() {
            return mPage.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return SimpleDateFormat.getDateInstance().format(mPage.get(position).getTimeInMillis());
        }
    }
}
