package com.shuneault.agdq2016;

import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;

import com.shuneault.agdq2016.objects.GameSchedule;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sebast on 01/01/16.
 */
public class AsyncTaskGetDonation extends AsyncTask<Void, Void, String> {

    public interface OnThreadProgress {
        void onFinish(String donation);
    }

    private static final String URL_BASE_DONATION = "https://gamesdonequick.com";
    private static final String URL_DONATION_INDEX = "https://gamesdonequick.com/tracker/events/";
    private static final String RE_AMOUNT = "Donation Total:\\n(.+)\\.\\d+ \\(\\d+\\)";
    private static final String RE_LAST_EVENT_URL = "^<a href=\"(/tracker/event/.+?)\"";

    private final OnThreadProgress mListener;

    public AsyncTaskGetDonation(OnThreadProgress listener) {
        mListener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {
        Log.i("LOGCAT", "AsyncTaskGetDonation doInBackground");
        String strDonation = "";

        try {
            List<String> eventUrls = getEventUrls();
            int eventIndex = 0;
            while (eventIndex < eventUrls.size() && ("".equals(strDonation) || "$0".equals(strDonation))) {
                URL url = new URL(eventUrls.get(eventIndex++));
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setUseCaches(false);

                StringBuilder strCode = new StringBuilder();
                int b;
                while ((b = httpURLConnection.getInputStream().read()) != -1) {
                    strCode.append((char) b);
                }

                Pattern p = Pattern.compile(RE_AMOUNT, Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(strCode);
                while (m.find()) {
                    strDonation = Html.fromHtml(m.group(1)).toString();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("LOGCAT", "ERROR: " + e.getMessage());
        }

        return strDonation;
    }

    private List<String> getEventUrls() {
        List<String> strUrls = new ArrayList<>();

        try {
            URL url = new URL(URL_DONATION_INDEX);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setUseCaches(false);

            StringBuilder strCode = new StringBuilder();
            int b;
            while ( (b = httpURLConnection.getInputStream().read() ) != -1) {
                strCode.append((char)b);
            }


            Pattern p = Pattern.compile(RE_LAST_EVENT_URL, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
            Matcher m = p.matcher(strCode);
            while (m.find()) {
                strUrls.add(URL_BASE_DONATION + Html.fromHtml(m.group(1)).toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.i("LOGCAT", "ERROR: " + e.getMessage());
        }

        return strUrls;
    }

    @Override
    protected void onPostExecute(String donation) {
        // Only update if we have a result
        if (!"".equals(donation)) {
            mListener.onFinish(donation);
        }
    }
}
